# REST API Server for Open iT Base Data Warehouse

A REST API Server for accessing the Open iT Base Data Warehouse.
The REST API returns data in CSV format.

## Usage

```sh
openit-restapi C:/ProgramData/OpeniT/Data/database
```

Open a browser and go to [http://localhost:7361/datatypes/94](http://localhost:7361/datatypes/94).
Change the datatype number to a desired one.

To specify a date, add `?date=YYYY-MM-DD&freq=day&periods=1` query string.

|    Query     |       Value         | Description |
|--------------|---------------------|-------------|
|    date      |     YYYY-MM-DD      | Start date. Default is current date.  |
|    freq      | day, month, or year | This is the aggregation level in Core Server database. Default is day. |
|   periods    |       integer       | Depending on the frequency, specify the number of days/months/years to get. Default is 1. |

To trigger a download of the CSV in the browser, add `?download=true` in the query string.

## Install

1. Install [NodeJS](https://nodejs.org/en/download/)
1. `git clone` this repository
1. Open terminal
1. Change directory to the cloned repository
1. Run

    ```sh
    npm install -g .
    ```

## Supported Datatypes

See `datatypes.json` to see supported datatypes.
To help add more datatypes, kindly submit a pull request.
Please observe proper naming convention and uniformity of classifications and measurements.
