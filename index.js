#!/usr/bin/env node
const package       = require('./package.json')
const argparse      = require('argparse')
const assert        = require('assert')
const cluster       = require('cluster')
const os            = require('os')
const fs            = require('fs')
const util          = require('util')
const moment        = require('moment')
const mr            = require('moment-range')
const express       = require('express')
const compression   = require('compression')
const papa          = require('papaparse')
const read          = require('./read')
const datatypes     = require('./datatypes.json')
const settings      = require('./settings')

function main() {
    configure()

    const db    = settings.db
    const port  = settings.port
    let app     = express()

    assert(fs.existsSync(db), 'database does not exist')
    
    mr.extendMoment(moment)
    app.use(compression())

    app.get('/datatypes/:id', async function(req, res) {
        try {
            assert(req.params.id !== undefined, 'datatype must be set')

            let dt          = req.params.id
            let date        = moment(req.query.date)
            let periods     = req.query.periods === undefined ? 1 : +req.query.periods
            let freq        = req.query.freq === undefined ? 'day' : req.query.freq
            let datatype    = datatypes[dt]

            assert(datatype, 'unknown datatype')

            let classifications = datatype.classifications
            let measurements    = datatype.measurements
            let header          = classifications.concat(measurements)

            let filename        = [dt, freq, date.format('YYYY-MM-DD'), periods].join('-') + '.csv'
            let range           = moment.rangeFromInterval(freq, periods - 1, date)

            res.set('Content-Type', 'text/plain')

            if (req.query.download) {
                res.set('Content-Type', 'text/csv')
                res.set('Content-Disposition', util.format('filename="%s"', filename))
            }

            res.write(papa.unparse([header, []]))

            for (const m of range.by(freq)) {
                let csv = await read(m, db, dt, freq, classifications.length)
                if (csv)
                    res.write(csv)
            }

            res.status(200)
            res.end()
        } catch (e) {
            res.status(400)
            res.end(json({
                error: true,
                name: e.name,
                message: e.message
            }))
        }
    })

    app.listen(port)
}

function json(object) {
    return JSON.stringify(object, null, 4)
}

function parse() {
    let { description, version } = package
    let parser = new argparse.ArgumentParser({ description })
    parser.add_argument('-V', '--version', { help: 'show version information and exit', action: 'version', version })
    parser.add_argument('database',        { help: 'database path'  })
    parser.add_argument('-p',              { metavar: 'PORT', help: `port number (default ${settings.port})`,
                                             default: settings.port })
    return parser.parse_args()
}

function configure() {
    let args      = parse()
    settings.port = args.p
    settings.db   = args.database
}

if (cluster.isMaster) {
    configure()
    for (let i = 0; i < os.cpus().length; i++)
        cluster.fork()
} else {
    main()
}
