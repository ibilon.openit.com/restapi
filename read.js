const fs        = require('fs')
const papa      = require('papaparse')
const zlib      = require('zlib')
const readline  = require('readline')

function read(m, db, dt, freq, classification_len) {
    let year  = m.year()
    let month = m.month() + 1
    let day   = m.date()
    let path  = null

    if (freq == 'year') {
        path = [db, dt, 'M', year, 'data'].join('/')
    } else if (freq == 'month') {
        path = [db, dt, 'M', year, month, 'data'].join('/')
    } else if (freq == 'day') {
        path = [db, dt, 'M', year, month, day, 'data.gz'].join('/')
    } else {
        throw new Error('invalid frequency')
    }

    return new Promise(function(resolve, reject) {
        if (fs.existsSync(path)) {
            let file = fs.createReadStream(path)

            if (path.substr(-3) == '.gz')
                file = file.pipe(zlib.createGunzip())

            let rl   = readline.createInterface({ input: file })
            let buff = ""
            let c    = ""
            let m    = ""
            let n    = 0
            let rows = []

            rl.on('line', function(line) {
                n++
                if (n % 2 == 1) {
                    c = stripClassification(line.split(':'), classification_len)
                } else {
                    m = line.split(':')
                    let row = c.concat(m)
                    rows.push(row)
                }
            })

            rl.on('close', function() {
                file.close()
                let csv = papa.unparse(rows)
                resolve(csv)
            })
        } else {
            resolve()
        }
    })
}

function stripClassification(fields, n) {
    let output = fields.slice(0, 3)
    for (let i = 4; i < n * 2 - 3; i += 2)
        output.push(fields[i].trim())
    return output
}

module.exports = read
